/*-----------------------------------------------------------------------------
A simple echo bot for the Microsoft Bot Framework. 
-----------------------------------------------------------------------------*/

var restify = require("restify");
var builder = require("botbuilder");
var botbuilder_azure = require("botbuilder-azure");
var admin = require("firebase-admin");
var utils = require("./util");
var serviceAccount = require("./firebase.json");
var inMemoryStorage = new builder.MemoryBotStorage();
var teams = require("botbuilder-teams");

var userID, tenantID;

//Initialize firebase app
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://devbot-76b59.firebaseio.com"
});

var db = admin.database();

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
  console.log("%s listening to %s", server.name, server.url);
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
  appId: "0368e28f-17cf-439e-9458-39723c708427",
  appPassword: "bfqvDPZ83(rncXSVH185*@[",
  openIdMetadata: process.env.BotOpenIdMetadata
});

// !!!!!! switch back after testing locally with teams
// this links to benchy dev - for development and testing in Microsoft Teams
// var connector = new builder.ChatConnector({
//   appId: "9638fe41-51d6-4d60-98d6-aa46cbf8c80e",
//   appPassword: "xjxgiS5324;;hlTYKVIW0(%",
//   openIdMetadata: process.env.BotOpenIdMetadata
// });

// Listen for messages from users
server.post("/api/messages", connector.listen());

// Create your bot with a function to receive messages from the user
var bot = new builder.UniversalBot(connector).set("storage", inMemoryStorage); // Register in memory storage

// --== ROOT DIALOG ==--
bot
  .dialog("/", function(session, args, next) {
    session.userData = {};

    //these variables are used when we create a 1on1 chat for registration
    if (session.message.source === "msteams") {
      tenantID =
        typeof session.message.sourceEvent.tenant !== "undefined"
          ? session.message.sourceEvent.tenant.id
          : "";
      userID = session.message.user.id;
    }

    if (session.message.address.conversation.conversationType !== "personal") {
      // say Hi
      let aNames = session.message.user.name.split(",");
      session.send(
        "Hi, " + aNames[aNames.length - 1] + ", I will message you!"
      );

      // START PRIVATE CHAT
      session.beginDialog("privateChat");
    } else {
      // ALREADY IN PRIVATE CHAT
      session.beginDialog("startOfPrivateChat");
    }
  })
  .triggerAction({
    matches: /^benchy$/i
  });

// --== REGISTRATION ==--
bot
  .dialog("registration", [
    function(session, args, next) {
      builder.Prompts.confirm(session, "Are you currently benched? (Y/N)");
    },
    function(session, results, next) {
      session.userData.benched = results.response;
      if (results.response == false) {
        builder.Prompts.text(
          session,
          "Please tell me your current project name."
        );
      } else {
        next();
      }
    },
    function(session, results, next) {
      session.userData.projectName =
        typeof results.response === "undefined"
          ? "Bench"
          : results.response.toUpperCase();
      session.send("Please enter your skills in a comma separated list. ");
      builder.Prompts.text(session, "E.g. (ABAP,UI5,Gateway)");
    },
    function(session, results, next) {
      var skillModel = [];
      var input = results.response;
      aInput = input.split(",");
      aInput.forEach(e => {
        skillModel.push({ skillName: e.trim().toUpperCase() });
      });
      session.userData.skills = skillModel;

      //the userId will be used if someone wants to contact someone on the bench in a one on one chat
      session.userData.userId = session.message.user.id;
      session.userData.username = session.message.user.name.toUpperCase();

      // async request so type whilst waiting
      session.sendTyping();

      // async request so type whilst waiting
      session.sendTyping();
      // ASYNC - save the skills, adding the user underneath each
      let aSkills = session.userData.skills;
      let userid = session.userData.userId;
      let oUserData = {
        [userid]: session.userData
      };

      let aPromises = [];
      // Save Skill
      aSkills.forEach(skill => {
        aPromises.push(
          new Promise((resolve, reject) => {
            db.ref("/Skills/" + skill.skillName)
              .set(oUserData)
              .then(data => {
                resolve();
              });
          })
        );
      });

      // Save User Info
      aPromises.push(
        db.ref("/Users/" + session.message.user.id).set(session.userData)
      );

      session.userData = {};
      session.endDialog();

      Promise.all(aPromises).then(() => {
        session.send("Great, your details have been captured!");

        // SHOW MAIN MENU
        session.beginDialog("mainmenu");
      });
    }
  ])
  .triggerAction({
    matches: /^register$|^registration$/i
  })
  .cancelAction("cancelAction", "Okay, no problem!", {
    matches: /^nevermind$|^cancel$/i
  });

// --== MAIN MENU ==--
bot
  .dialog("mainmenu", [
    function(session) {
      builder.Prompts.choice(
        session,
        "What would you like to do?",
        "Bench Report|Find Someone|Who is at project |Update Info|View my Info|Help|Nothing",
        {
          listStyle: builder.ListStyle.button
        }
      );
    },
    function(session, results) {
      var answer = results.response.entity.trim();
      switch (answer) {
        case "Bench Report":
          session.beginDialog("benchreport");
          break;
        case "Find Someone":
          session.beginDialog("whereisprompt");
          break;
        case "Who's at project":
          session.beginDialog("whosatprojectprompt");
          break;
        case "Update Info":
          session.beginDialog("updateinfoprompt");
          break;
        case "View my Info":
          session.beginDialog("myinfo");
          break;
        case "Help":
          session.beginDialog("help");
          break;
        default:
          let goodbye = utils.returnRandomGoodbye();
          session.endDialog(goodbye);
      }
    }
  ])
  .triggerAction({
    matches: /^main.*menu$|^menu$/i,
    onSelectAction: (session, args, next) => {
      // Add the dialog to the top of the dialog stack
      // (override the default behavior of replacing the stack)
      session.beginDialog(args.action, args);
    }
  });

// --== BENCH REPORT ==--
bot
  .dialog("benchreport", [
    function(session) {
      var list = [];
      db.ref("/Users")
        .orderByChild("benched")
        .equalTo(true)
        .once("value", function(snapshot) {
          snapshot.forEach(function(childSnapshot) {
            list.push(childSnapshot.val().username);
          });
        })
        .then(function() {
          if (list.length > 0) {
            let title =
              "The following are on the bench, please select one to start a conversation:";
            // list.push("Select All");
            session.beginDialog("showPeople", [title, list]);
          } else {
            session.send("No one is currently registered as on the Bench!");
          }
        });
    }
  ])
  .triggerAction({
    matches: /^who.*bench.*|^who's.*bench.*|^bench.*/i
  });

// Dialog to display clickable list of people
bot
  .dialog("showPeople", [
    function(session, aArgs) {
      session.send(
        "Please select an option from the list, if you would like to do something else type 'menu' or type 'cancel' to quit."
      );
      let title = aArgs[0],
        list = aArgs[1];
      builder.Prompts.choice(session, title, list, {
        listStyle: builder.ListStyle.button
      });
    },
    function(session, results, next) {
      // results.response.entity = username
      session.send("you have selected: " + results.response.entity);
      session.beginDialog("privateChat", results);
    }
  ])
  .cancelAction("cancelAction", "Ok, no problem!", {
    matches: /^nevermind$|^cancel$/i
  });

// --== START PRIVATE CHAT ==--
bot.dialog("privateChat", [
  function(session, results) {
    let bContinuePrivate = true,
      sError = "";

    if (typeof results !== "undefined") {
      if (results.response.entity === "Select All") {
        sError = "I can only handle 1:1 chats at the moment! :(";
        bContinuePrivate = false;
      }
    }

    if (bContinuePrivate) {
      var address = {
        channelId: "msteams",
        user: { id: session.message.user.id },
        channelData: {
          tenant: {
            id:
              session.message.source === "msteams"
                ? session.message.sourceEvent.tenant.id
                : ""
          }
        },
        bot: {
          id: session.message.address.bot.id,
          name: session.message.address.bot.name
        },
        serviceUrl: session.message.address.serviceUrl,
        useAuth: true
      };

      bot.beginDialog(address, "startOfPrivateChat", "initPrivate");
    } else {
      session.send(sError);
    }
  }
]);

bot.dialog("startOfPrivateChat", function(session, args, next) {
  // async request so type whilst waiting
  session.sendTyping();
  db.ref("/Users/" + session.message.user.id)
    .once("value")
    .then(function(snapshot) {
      var username = (snapshot.val() && snapshot.val().userId) || "Anonymous";
      if (username === "Anonymous") {
        session.send("Lets get you registered: ");
        session.beginDialog("registration");
      } else {
        // already registered - ask what they want to do
        session.userName = username;
        session.replaceDialog("mainmenu");
      }
    });
});

// --== WHERE IS - SEARCH FOR USERS==--
bot
  .dialog("whereis", function(session, args, next) {
    var msg = session.message.text.trim();
    var aWords = msg.split(" ");

    // if they haven't specified a person straight away
    // -- give them a dialog
    if (aWords.length === 1) {
      session.beginDialog("whereisprompt");
    } else {
      // else skip the dialog and search
      // --search with last word in the sentence
      var searchString = aWords[aWords.length - 1];

      // async request so type whilst waiting
      session.sendTyping();
      if (searchString === "") {
        session.beginDialog("whereisprompt");
      } else {
        searchUsersDB(searchString, "username").then(function(list) {
          if (list.length > 0) {
            let title = "Here is what I found:";
            let aPpl = utils.returnUsernameAndProject(list);
            session.beginDialog("showPeople", [title, aPpl]);
          } else {
            session.beginDialog("whereisprompt");
          }
        });
      }
    }
  })
  .triggerAction({
    matches: /^where.*is.*|^where's.*|^wheres.*|find.*/i
  });

bot
  .dialog("whereisprompt", [
    function(session) {
      builder.Prompts.text(session, "Who are you searching for?");
    },
    function(session, results) {
      var searchString = results.response.toUpperCase();

      // async request so type whilst waiting
      session.sendTyping();
      searchUsersDB(searchString, "username").then(function(list) {
        if (list.length > 0) {
          let title = "Here is what I found:";
          let aPpl = utils.returnUsernameAndProject(list);
          session.beginDialog("showPeople", [title, aPpl]);
        } else {
          session.endDialog("Didn't find anyone by the name " + searchString);
        }
      });
    }
  ])
  .cancelAction("cancelAction", "Ok, no problem!", {
    matches: /^nevermind$|^cancel$/i
  });

// --== WHO'S AT - RETURN PEOPLE ON PROJECTS ==--
bot
  .dialog("whosatproject", function(session, args, next) {
    var msg = session.message.text.trim();
    var aWords = msg.split(" ");

    // if they haven't specified a person straight away
    // -- give them a dialog
    if (aWords.length === 1) {
      session.beginDialog("whosatprojectprompt");
    } else {
      // else skip the dialog and search
      // --search with last word in the sentence
      var searchString = aWords[aWords.length - 1].trim();

      // async request so type whilst waiting
      session.sendTyping();
      searchUsersDB(searchString, "projectName").then(function(list) {
        if (list.length > 0) {
          let title = "Here is what I found:";
          let aPpl = utils.returnUsernameAndProject(list);
          session.beginDialog("showPeople", [title, aPpl]);
        } else {
          session.beginDialog("whosatprojectprompt");
        }
      });
    }
  })
  .triggerAction({
    matches: /who.*project.*|who's.*project.*|whos.*project.*|who's.*at.*|whos.*at.*|who's.*on.*|whos.*on.*/i
  });

bot
  .dialog("whosatprojectprompt", [
    function(session) {
      builder.Prompts.text(session, "What project are you looking for?");
    },
    function(session, results) {
      var searchString = results.response.toUpperCase();

      // async request so type whilst waiting
      session.sendTyping();
      searchUsersDB(searchString, "projectName").then(function(list) {
        if (list.length > 0) {
          let title = "Here is what I found:";
          let aPpl = utils.returnUsernameAndProject(list);
          session.beginDialog("showPeople", [title, aPpl]);
        } else {
          session.endDialog(
            "Didn't find anyone on the project " + searchString
          );
        }
      });
    }
  ])
  .cancelAction("cancelAction", "Ok, no problem!", {
    matches: /^nevermind$|^cancel$/i
  });

// --== WHO'S GOT SKILLS - RETURN PEOPLE ON PROJECTS ==--
bot
  .dialog("whosgotskills", function(session, args, next) {
    var msg = session.message.text.trim();
    var aWords = msg.split(" ");

    // if they haven't specified a skill straight away
    // -- give them a dialog
    if (aWords.length === 1) {
      session.beginDialog("whosgotskillsprompt");
    } else {
      // remove common words
      let aCommonSkillWords =
        "skill, skills, developer, programmer, consultant, in, at";
      let filteredWords = utils.getUncommon(msg, aCommonSkillWords);

      // else skip the dialog and search
      // --search with last word in the sentence
      var searchString = filteredWords[filteredWords.length - 1];

      // async request so type whilst waiting
      session.sendTyping();
      searchSkillsDB(searchString).then(function(list) {
        if (list.length > 0) {
          let title =
            "Here is what I found for people with " + searchString + " skills:";
          let aPpl = utils.returnUsernameAndProject(list);
          session.beginDialog("showPeople", [title, aPpl]);
        } else {
          session.beginDialog("whosgotskillsprompt");
        }
      });
    }
  })
  .triggerAction({
    matches: /^who.*skill.*|^who's.*skill.*|^whos.*skill.*|^who's.*got.*|^whos.*got.*|^skill.*search.*|^search.*skill.*/i
  });

bot
  .dialog("whosgotskillsprompt", [
    function(session) {
      builder.Prompts.text(
        session,
        "What skill are you looking for? (please enter one - e.g. ABAP)"
      );
    },
    function(session, results) {
      var searchString = results.response.toUpperCase();

      // async request so type whilst waiting
      session.sendTyping();
      searchSkillsDB(searchString).then(function(list) {
        if (list.length > 0) {
          let title =
            "Here is what I found for people with " + searchString + " skills:";
          let aPpl = utils.returnUsernameAndProject(list);
          session.beginDialog("showPeople", [title, aPpl]);
        } else {
          session.endDialog(
            "Didn't find anyone with the skill " + searchString
          );
        }
      });
    }
  ])
  .cancelAction("cancelAction", "Ok, no problem!", {
    matches: /^nevermind$|^cancel$/i
  });

// --== UPDATE INFO ==--
bot
  .dialog("updateinfoprompt", function(session, results) {
    session.send("Let's update your info:");
    session.beginDialog("registration");
  })
  .triggerAction({
    matches: /^update.*/i
  })
  .cancelAction("cancelAction", "Ok, no problem!", {
    matches: /^nevermind$|^cancel$/i
  });

// --== RETURN YOUR INFO ==--
bot
  .dialog("myinfo", function(session, args, next) {
    // async request so type whilst waiting
    session.sendTyping();
    searchForUser(session.message.user.id)
      .then(function(oUser) {
        if (oUser !== "none") {
          let title = "Here are your details:";
          let sDetails = utils.returnUserDetails(oUser);
          session.send(title);
          session.send(sDetails);
        } else {
          session.endDialog(
            "Can't seem to find you. Type 'Register' to register your details."
          );
        }
      })
      .catch(err => {});
  })
  .triggerAction({
    matches: /^my.*info.*|view.*my.*info.*|my.*detail.*|my.*record.*/i
  })
  .cancelAction("cancelAction", "Okay, no problem!", {
    matches: /^nevermind$|^cancel$|^thanks$/i
  });

// --== HELP - RETURN COMMANDS WITH EXPLANATION ==--
bot
  .dialog("help", function(session, results) {
    let sHelp = "### Typing the following commands will: ###";
    sHelp += "\n* **cancel** - this will cancel the current request ";
    sHelp += "\n* **main menu** - this will give you a list of options ";
    sHelp += "\n* **register** - this will register your details ";
    sHelp += "\n* **update** - this will allow you to update your details";
    sHelp += "\n* **my info** - this show you your details";
    sHelp +=
      "\n* **who is at project X OR who's at project X ** - this will search for everybody at project X";
    sHelp +=
      "\n* **where is X** - this will search for a person with the surname of X";
    sHelp += "\n* **who's on the bench** - this will display the bench report";
    sHelp +=
      "\n* **help** - this will display a list of commands and what they do";
    session.send(sHelp);
    session.endDialog();
  })
  .triggerAction({
    matches: /^help.*/i
  })
  .cancelAction("cancelAction", "Okay, no problem!", {
    matches: /^nevermind$|^cancel$|^thanks$/i
  });
// --== HELP - RETURN COMMANDS WITH EXPLANATION ==--
bot
  .dialog("goodbye", function(session, results) {
    let sGoodbye = "Speak soon!";
    session.endDialog(sGoodbye);
  })
  .triggerAction({
    matches: /^goodbye$/i
  });

function searchUsersDB(searchString, child) {
  var list = [];
  return new Promise((resolve, reject) => {
    searchString = searchString.toUpperCase().trim();
    db.ref("/Users")
      .orderByChild(child)
      .startAt(searchString)
      .endAt(searchString + "\uf8ff")
      .once("value", snapshot => {
        snapshot.forEach(function(childSnapshot) {
          list.push(childSnapshot.val());
        });
        resolve(list);
      });
  });
}

function searchSkillsDB(skill) {
  var list = [];
  return new Promise((resolve, reject) => {
    skill = skill.toUpperCase().trim();
    db.ref("/Skills/" + skill).once("value", snapshot => {
      snapshot.forEach(function(childSnapshot) {
        list.push(childSnapshot.val());
      });
      resolve(list);
    });
  });
}

function searchForUser(userId) {
  return new Promise((resolve, reject) => {
    db.ref("/Users/" + userId)
      .once("value")
      .then(function(snapshot) {
        var username = (snapshot.val() && snapshot.val().userId) || "Anonymous";
        if (username === "Anonymous") {
          resolve("none");
        } else {
          resolve(snapshot.val());
        }
      });
  });
}
